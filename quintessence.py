from bs4 import BeautifulSoup
import settings
import requests


class QJApi:

    unparsed_xml = ''

    def __init__(self):
        self.base_url = settings.qj_url
        self.username = settings.qj_username
        self.api_key = settings.qj_password

    def get_product_data(self):
        """
        Return dict of product data
        """
        self.get_xml()
        return self.parse_xml()

    def get_xml(self):
        """
        Get XML from API.
        """

        data = {'sku_name': '*', 'uname': self.username, 'pass': self.api_key}
        r = requests.post(self.base_url, data=data)
        self.unparsed_xml = r.text

    def parse_xml(self) -> dict:
        """
        Parse XML and return a dict.  The key will be the SKU
        and value will be a dictionary with the QOH, Description,
        price, and list of image urls.
        """

        soup = BeautifulSoup(self.unparsed_xml, 'html.parser')
        product_dict = {}

        for product in soup.find_all('product'):
            product_images = []

            for image in product.images:
                product_images.append(image.string)

            product_dict[product['sku']] = {
                'qoh': product['stockqty'],
                'description': product['product_title'],
                'price': product['price'],
                'images': product_images
            }

        return product_dict
