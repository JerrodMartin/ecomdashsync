import requests
import settings


class EcommdashApiBase:

    def __init__(self):
        self.headers = {
            'ecd-subscription-key': settings.ecd_subscription_key,
            'Ocp-Apim-Subscription-Key': settings.ocp_apim_subscription_key,
        }

        self.api_url = settings.ecommdash_api_url

    def build_url(self, path: str) -> str:
        """
        Return complete URL constructed from the API Url with the API path
        added.
        """
        return f'{self.api_url}{path}'

    def post(self, url: str, payload) -> requests.models.Response:
        """
        Post data to API and return requests response object

        Payload will be converted to JSON format automatically
        """

        return requests.post(
            url,
            json=payload,
            headers=self.headers
        )

    def get(self, url: str, payload: dict) -> requests.models.Response:
        """
        Get data from API and return requests response object.

        The requests library will take care of forming the URL
        with the payload.  More info here:
        http://docs.python-requests.org/en/master/user/quickstart/#passing-parameters-in-urls
        """

        return requests.get(
            url,
            params=payload,
            headers=self.headers
        )

    def put(self, url: str, payload: dict) -> requests.models.Response:
        """
        Put data to API and return requests repsone object.
        """

        return requests.put(
            url,
            json=payload,
            headers=self.headers
        )


class Inventory(EcommdashApiBase):

    def create_product(self, **params)-> requests.models.Response:
        """
        Create product using Ecomdash API and return requests response object.

        Available params can be found here:
        https://ecomdash.portal.azure-api.net/docs/services/56a62d3be7b19d09a48c9b72/operations/57bf53017b829e0da0f398f0?
        """

        url = self.build_url('Inventory')

        return self.post(url, params)

    def update_inventory(self, inv_list: list) -> requests.models.Response:
        """
        Input a list of dict(s) using the below format
        and update the Ecomdash API. Return requests response object

        [
            {
                "Sku": <sku>,
                "Quantity": <qoh>,
                "WarehouseId": <warehouseid>
            }
        ]
        """

        url = self.build_url('inventory/updateQuantityOnHand')

        return self.post(url, inv_list)

    def update_price(self, sku: str, price: float, description: str) -> requests.models.Response:
        """
        Update price on Ecomdash API.  The below information is required
        in the PUT method.

         {
            "Id": 114867786,
            "Sku": "TESTSKU",
            "Name": "TESTING UPDATE",
            "SupplierInformation": {
                "Price": 5.02,
            },
        }
        """

        url = self.build_url('Inventory')
        product_details = self.get_product_details(sku).json()

        product_update = {
            'Id': product_details['Id'],
            'Sku': product_details['Sku'],
            'Name': description,
            'SupplierInformation': {
                'Price': price
            },
        }

        return self.put(url, product_update)

    def get_product_details(self, sku: str) -> requests.models.Response:
        """
        Get product details for a single SKU.  Returns a dict in the following
        format:

        {
              "Id": 114867786,
              "Sku": "TESTSKU",
              "QuantityOnHand": 10,
              "Warehouses": [{
                "ProductId": 114867786.0,
                "WarehouseId": 37131.0,
                "QuantityOnHand": 0.0,
                "WarehouseSKU": "TESTSKU",
                "IsActive": true,
                "IncludeInSyncBalance": true
              }]
        }
        """

        url = self.build_url('product')
        param = {'sku': sku}

        return self.get(url, param)

    def delete_product(self, sku: str, description: str) -> requests.models.Response:
        """
        Delete SKU with Ecomdash API.  Below information is required.

        {
            "Id": 114867786,
            "Sku": "TESTSKU",
            "Name": "TESTING UPDATE",
            "IsActive": False
            },
        }
        """

        url = self.build_url('Inventory')
        product_details = self.get_product_details(sku).json()

        product_update = {
            'Id': product_details['Id'],
            'Sku': product_details['Sku'],
            'Name': description,
            'IsActive': False
        }

        return self.put(url, product_update)
