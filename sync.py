#!/usr/bin/python


from quintessence import QJApi
from ecomdash import Inventory
import logging
import pickle
import settings
import socket


logger = settings.logger


def is_api_up(api_domain: str) -> bool:
    """
    Check if API host is reachable
    """

    if api_domain.startswith('https'):
        port = 443
    else:
        port = 80

    host = api_domain.split('/')[2]

    try:
        s = socket.socket()
        s.connect((host, port))
        s.close()
        return True

    except socket.error:
        return False


# -------------------------------------------------------------------
# Quintessence

class SyncQJ:

    qj_api = QJApi()
    ecom_api = Inventory()

    def __init__(self):
        self.fresh_product_data = self.qj_api.get_product_data()
        self.stale_product_data = self.unpickle_data()

    def inventory_to_create(self) -> dict:
        """
        Compare fresh and stale data and determine
        if any new products need to be created in
        Ecomdash.  Return dict with SKU as key and
        dict of description and image urls as values
        """

        inv_to_create = {}

        for sku, info in self.fresh_product_data.items():
            if sku in self.stale_product_data:
                continue

            inv_to_create[sku] = {
                'description': info['description'],
                'images': info['images']
            }

        return inv_to_create

    def create_products(self) -> None:
        """
        Iterate through fresh products and create them via
        the Ecommdash API
        """

        prod_dict = self.inventory_to_create()

        for sku, info in prod_dict.items():
            logging.info(f'Creating {sku}')

            params = {
                'Sku': sku,
                'Name': info['description'],
                'Images': []
            }

            for image in info['images']:
                params['Images'].append(
                    {'Url': image}
                )

            self.ecom_api.create_product(**params)

    def delete_products(self) -> None:
        """
        Iterate through stale products and delete any that are not
        in fresh products.  Does not delete items if fresh_product_data
        is empty.
        """

        if self.fresh_product_data:
            for sku, info in self.stale_product_data.items():
                if sku in self.fresh_product_data:
                    continue

                self.ecom_api.delete_product(sku, info['description'])
                logging.info(f'Deleting {sku}')

        else:
            logging.warning(
                """
                Not deleting products since there are no fresh
                products to compare with.
                """
            )

    def update_products(self) -> None:
        """
        Compare fresh and stale product data and update QOH
        and/or price if necessary using the Ecommdash API.
        Assumes all fresh products have already been created
        on Ecomdash.

        API allows a list of QOH to update however we must update
        each price separately.
        """
        inv_to_update_qoh = []

        for sku, info in self.fresh_product_data.items():
            new_qoh = info['qoh']
            warehouseid = settings.qj_warehouse_id
            new_price = float(info['price'].strip('$'))
            description = info['description']

            try:
                old_price = float(self.stale_product_data[sku]['price'].strip('$'))

                if self.stale_product_data[sku]['qoh'] != new_qoh:
                    product = {
                        "Sku": sku,
                        "Quantity": new_qoh,
                        "WarehouseId": warehouseid
                    }

                    inv_to_update_qoh.append(product)
                    logging.info(f'QOH for {sku} will be updated to {new_qoh}')

                if old_price != new_price:
                    self.ecom_api.update_price(
                        sku,
                        new_price,
                        description
                    )
                    logging.info(f'Price for {sku} will be updated to {new_price}')

            except KeyError:
                product = {
                    "Sku": sku,
                    "Quantity": new_qoh,
                    "WarehouseId": warehouseid
                }

                inv_to_update_qoh.append(product)
                logging.info(f'QOH for {sku} will be updated to {new_qoh}')

                self.ecom_api.update_price(
                    sku,
                    new_price,
                    description
                )

                logging.info(f'Price for {sku} updated to {new_price}')

        self.ecom_api.update_inventory(inv_to_update_qoh)

    @staticmethod
    def unpickle_data() -> dict:
        """
        Get pickled data and return empty dict if there is none.
        """
        try:
            with open(r"{}/inventory_data.pickle".format(settings.project_root), "rb") as f:
                return pickle.load(f)

        except FileNotFoundError:
            return {}

    def pickle_data(self) -> None:
        """
        Pickle fresh items to use for next time
        """
        with open(r'{}/inventory_data.pickle'.format(settings.project_root), 'wb') as f:
            pickle.dump(self.fresh_product_data, f)


if __name__ == '__main__':

    if is_api_up(settings.ecommdash_api_url):
        sync_qj = SyncQJ()
        sync_qj.create_products()
        sync_qj.update_products()
        sync_qj.delete_products()
        sync_qj.pickle_data()
        logging.info('Sync complete')

    else:
        logging.error(f'Can not reach {settings.ecommdash_api_url}; skipping sync.')
